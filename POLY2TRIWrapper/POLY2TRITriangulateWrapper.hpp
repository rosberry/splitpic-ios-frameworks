//
//  POLY2TRITriangulateWrapper.h
//  POLY2TRIWrapper
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifndef __POLY2TRIWRAPPER__POLY2TRITRIANGULATEWRAPPER__
#define __POLY2TRIWRAPPER__POLY2TRITRIANGULATEWRAPPER__

#ifdef __cplusplus

#include <CoreGraphics/CGGeometry.h>
#include <vector>

class POLY2TRITriangulateWrapper
{
private:
    POLY2TRITriangulateWrapper() {};
    ~POLY2TRITriangulateWrapper() {};
    
public:
    
    /**
     Perform triangulate operation for polygon of input points and hole polygon, if available.
     
     @param input vector of polygon points.
     
     @param input vector of hole polygon points.
     
     @param output if operation completed - add triangle points to output (use GL_TRIANGLES for drawing)
     
     @return true if operation succeeded, otherwise false.
     */
    
    static bool triangulate(const std::vector<CGPoint>& input, const std::vector<CGPoint>& hole, std::vector<CGPoint>& output);
    
};

#endif /* __cplusplus */
#endif /* defined(__POLY2TRIWRAPPER__POLY2TRITRIANGULATEWRAPPER__) */
