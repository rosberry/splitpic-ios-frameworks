//
//  POLY2TRITriangulateWrapper.cpp
//  POLY2TRIWrapper
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#include "POLY2TRITriangulateWrapper.hpp"
#include "poly2tri.h"

bool POLY2TRITriangulateWrapper::triangulate(const std::vector<CGPoint>& input, const std::vector<CGPoint>& hole, std::vector<CGPoint>& output)
{
    bool result = false;
    
    if (!input.size() || !hole.size())
    {
        return false;
    }
    
    std::vector<p2t::Point *> p2t_points, p2t_hole_points;
    
    for (auto p: input)
    {
        p2t_points.push_back(new p2t::Point(p.x, p.y));
    }
    
    for (auto p: hole)
    {
        p2t_hole_points.push_back(new p2t::Point(p.x, p.y));
    }
    
    p2t::CDT cdt(p2t_points);
    cdt.AddHole(p2t_hole_points);
    cdt.Triangulate();
    
    auto tris = cdt.GetTriangles();
    
    for (auto p: tris)
    {
        p2t::Point* a = p->GetPoint(0);
        p2t::Point* b = p->GetPoint(1);
        p2t::Point* c = p->GetPoint(2);
        
        output.push_back(CGPointMake(a->x, a->y));
        output.push_back(CGPointMake(b->x, b->y));
        output.push_back(CGPointMake(c->x, c->y));
    }
    
    result = output.size() > 0;
    
    while(!p2t_points.empty()) delete p2t_points.back(), p2t_points.pop_back();
    while(!p2t_hole_points.empty()) delete p2t_hole_points.back(), p2t_hole_points.pop_back();
    
    return result;
}
