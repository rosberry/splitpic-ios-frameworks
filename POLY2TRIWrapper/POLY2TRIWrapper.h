//
//  POLY2TRIWrapper.h
//  POLY2TRIWrapper
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifdef __cplusplus
#define POLY2TRIWRAPPER_EXTERN extern "C"
#else
#import <UIKit/UIKit.h>
#define POLY2TRIWRAPPER_EXTERN FOUNDATION_EXPORT
#endif

//! Project version number for POLY2TRIWrapper.
POLY2TRIWRAPPER_EXTERN double POLY2TRIWrapperVersionNumber;

//! Project version string for POLY2TRIWrapper.
POLY2TRIWRAPPER_EXTERN const unsigned char POLY2TRIWrapperVersionString[];

#import <POLY2TRIWrapper/POLY2TRITriangulateWrapper.hpp>
