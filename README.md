**GEOSWrapper** - C++ wrapper for GEOS library (http://trac.osgeo.org/geos/).

Currently support only polygon buffering - `GEOSPolygonBuffer::buffer(...)`.

---

**POLY2TRITriangulateWrapper** - C++ wrapper for poly2tri library (https://code.google.com/p/poly2tri/).

Perform polygone with hole triangulation - `POLY2TRITriangulateWrapper::triangulate(...)`.