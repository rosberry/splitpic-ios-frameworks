//
//  POLY2TRIWrapperTests.m
//  POLY2TRIWrapperTests
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <POLY2TRIWrapper/POLY2TRIWrapper.h>

NS_INLINE BOOL vectorContaintPoint(std::vector<CGPoint> &vector, CGPoint &point)
{
    for (auto p: vector)
    {
        if (CGPointEqualToPoint(p, point))
        {
            return YES;
        }
    }
    NSCAssert(NO, @"vector not contained point: {%.0f, %.0f}", point.x, point.y);
    return NO;
}

@interface POLY2TRIWrapperTests : XCTestCase

@end

@implementation POLY2TRIWrapperTests

- (void)testNonCrashForEmptyVectors
{
    auto input = std::vector<CGPoint>();
    auto hole = std::vector<CGPoint>();
    auto output = std::vector<CGPoint>();
    
    POLY2TRITriangulateWrapper::triangulate(input, hole, output);
}

- (void)testTriangulation
{
    std::vector<CGPoint> input = {
        CGPointMake(0.0f, 0.0f),
        CGPointMake(10.0f, 0.0f),
        CGPointMake(10.0f, 10.0f),
        CGPointMake(0.0f, 10.0f)
    };
    
    std::vector<CGPoint> hole = {
        CGPointMake(2.0f, 2.0f),
        CGPointMake(8.0f, 2.0f),
        CGPointMake(8.0f, 8.0f),
        CGPointMake(2.0f, 8.0f)
    };
    
    auto output = std::vector<CGPoint>();
    
    POLY2TRITriangulateWrapper::triangulate(input, hole, output);
    
    XCTAssert(output.size(), @"output should contain points!");
    XCTAssert(output.size() % 3 == 0, @"output should contain triangle points, so count should be multiples of 3");
    
    XCTAssert(vectorContaintPoint(output, input[0]));
    XCTAssert(vectorContaintPoint(output, input[1]));
    XCTAssert(vectorContaintPoint(output, input[2]));
    XCTAssert(vectorContaintPoint(output, input[3]));
    
    XCTAssert(vectorContaintPoint(output, hole[0]));
    XCTAssert(vectorContaintPoint(output, hole[1]));
    XCTAssert(vectorContaintPoint(output, hole[2]));
    XCTAssert(vectorContaintPoint(output, hole[3]));
}

@end
