//
//  GEOSPolygonBufferTriangulation.cpp
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 12/1/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#include "GEOSPolygonBufferTriangulation.hpp"

#include <geos/geom/CoordinateArraySequence.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/geom/LinearRing.h>
#include <geos/geom/Polygon.h>
#include <geos/geom/PrecisionModel.h>
#include <geos/operation/buffer/BufferOp.h>

#include <geos/triangulate/quadedge/QuadEdge.h>
#include <geos/triangulate/quadedge/QuadEdgeSubdivision.h>
#include <geos/triangulate/IncrementalDelaunayTriangulator.h>
#include <geos/triangulate/DelaunayTriangulationBuilder.h>

using namespace geos::geom;
using namespace geos::operation::buffer;
using namespace geos::triangulate;
using namespace geos::triangulate::quadedge;

bool GEOSPolygonBufferTriangulation::triangulate(const std::vector<CGPoint>& points, std::vector<CGPoint>& output, const GEOSPolygonBuffer::Parameters params)
{
    if (!points.size())
    {
        return false;
    }
    
    auto input = std::vector<CGPoint>(points);
    
    if (!CGPointEqualToPoint(input.front(), input.back()))
    {
        input.push_back(input.front());
    }
    
    auto geometryFactory = GeometryFactory::getDefaultInstance();
    auto coordinateSequence = new CoordinateArraySequence();
    auto linearRing = new LinearRing(coordinateSequence, geometryFactory);
    
    for (auto p: input)
    {
        coordinateSequence->add(Coordinate(p.x, p.y));
    }
    
    if (params.distance == 0.0f)
    {
        DelaunayTriangulationBuilder builder;
        builder.setSites(*linearRing);
        
        const GeometryFactory& geomFact(*GeometryFactory::getDefaultInstance());
        auto triangles = builder.getTriangles(geomFact);
        
        for (int i = 0; i < triangles->getNumGeometries(); i++)
        {
            auto geometry = triangles->getGeometryN(i);
            auto coordinates = geometry->getCoordinates();
            
            if (coordinates->size() >= 3)
            {
                for (int i = 0; i < 3; i++)
                {
                    auto coord = coordinates->getAt(i);
                    output.push_back(CGPointMake(coord.x, coord.y));
                }
            }
            
            delete coordinates;
        }
    }
    else
    {
        throw "Not implement!";
        /*int segments = params.segments != - 1 ?: BufferParameters::DEFAULT_QUADRANT_SEGMENTS;
        int cap = GEOSPolygonBuffer::geos_cap(params);
        
        auto buffer = linearRing->buffer(params.distance, segments, cap);
        auto bufferedPolygon = dynamic_cast<Polygon *>(buffer);
        
        if (bufferedPolygon->getNumInteriorRing() > 0)
        {
            auto line = (Geometry *)bufferedPolygon->getInteriorRingN(0);
            
            std::vector<Geometry *> holes;
            holes.push_back(line);
            
            auto poly = geometryFactory->createPolygon(linearRing, &holes);
            
            printf("%s", poly->toString().c_str());
            
            DelaunayTriangulationBuilder builder;
            builder.setSites(*poly);
            
            const GeometryFactory& geomFact(*GeometryFactory::getDefaultInstance());
            auto triangles = builder.getTriangles(geomFact);
            
            for (int i = 0; i < triangles->getNumGeometries(); i++)
            {
                auto geometry = triangles->getGeometryN(i);
                auto coordinates = geometry->getCoordinates();
                
                if (coordinates->size() >= 3)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        auto coord = coordinates->getAt(i);
                        output.push_back(CGPointMake(coord.x, coord.y));
                    }
                }
                
                delete coordinates;
            }
        }*/
        /*
        DelaunayTriangulationBuilder builder;
        builder.setSites(*bufferedPolygon);
        
        const GeometryFactory& geomFact(*GeometryFactory::getDefaultInstance());
        auto triangles = builder.getTriangles(geomFact);
        
        for (int i = 0; i < triangles->getNumGeometries(); i++)
        {
            auto geometry = triangles->getGeometryN(i);
            auto coordinates = geometry->getCoordinates();
            
            if (coordinates->size() >= 3)
            {
                for (int i = 0; i < 3; i++)
                {
                    auto coord = coordinates->getAt(i);
                    output.push_back(CGPointMake(coord.x, coord.y));
                }
            }
            
            delete coordinates;
        }
        */
    }
    
    /* Cleanup */
    delete linearRing;
    
    return output.size() > 0;
}
