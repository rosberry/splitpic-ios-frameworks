//
//  GEOSPolygonBuffer.cpp
//  GEOSWrapper
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#include "GEOSPolygonBuffer.hpp"

#include <geos/geom/CoordinateArraySequence.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/geom/LinearRing.h>
#include <geos/geom/Polygon.h>
#include <geos/geom/PrecisionModel.h>
#include <geos/operation/buffer/BufferOp.h>

using namespace geos::geom;
using namespace geos::operation::buffer;

int GEOSPolygonBuffer::geos_cap(const GEOSPolygonBuffer::Parameters& params)
{
    switch (params.cap) {
        case GEOSPolygonBuffer::Flat:
            return BufferParameters::CAP_FLAT;
        case GEOSPolygonBuffer::Round:
            return BufferParameters::CAP_ROUND;
        case GEOSPolygonBuffer::Square:
            return BufferParameters::CAP_SQUARE;
        default:
            return BufferParameters::CAP_FLAT;
    }
}

bool GEOSPolygonBuffer::buffer(const std::vector<CGPoint>& points, const Parameters params, std::vector<CGPoint>& output)
{
    bool result = false;
    
    if (!points.size() || params.distance == 0.0f)
    {
        return false;
    }
    
    auto input = std::vector<CGPoint>(points);
    
    if (!CGPointEqualToPoint(input.front(), input.back()))
    {
        input.push_back(input.front());
    }
    
    auto geometryFactory = GeometryFactory::getDefaultInstance();
    
    /* Create Line Ring and Buffered Poly from input points */
    auto coordinateSequence = new CoordinateArraySequence();
    
    for (auto p: input)
    {
        coordinateSequence->add(Coordinate(p.x, p.y));
    }
    
    int segments = params.segments != - 1 ?: BufferParameters::DEFAULT_QUADRANT_SEGMENTS;
    int cap = geos_cap(params);
    
    auto linearRing = new LinearRing(coordinateSequence, geometryFactory);
    auto buffer = linearRing->buffer(params.distance, segments, cap);
    auto bufferedPolygon = dynamic_cast<Polygon *>(buffer);
    
    if (bufferedPolygon->getNumInteriorRing() > 0)
    {
        auto line = bufferedPolygon->getInteriorRingN(0);
        auto lineCoordinates = line->getCoordinates();
        
        std::vector<Coordinate> coordinates;
        
        for (int i = 0; i < lineCoordinates->size(); i++)
        {
            coordinates.push_back(lineCoordinates->getAt(i));
        }
        
        /* Remove last coordinate, if it duplicate */
        
        if (coordinates.front() == coordinates.back())
        {
            coordinates.pop_back();
        }
        
        for (auto p: coordinates)
        {
            output.push_back(CGPointMake(p.x, p.y));
        }
        
        delete lineCoordinates;
        result = output.size() > 0;
    }
    
    delete bufferedPolygon;
    delete linearRing;
    
    return result;
}
