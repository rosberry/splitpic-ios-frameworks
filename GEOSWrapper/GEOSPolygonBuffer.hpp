//
//  GEOSPolygonBuffer.h
//  GEOSWrapper
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifndef __GEOSWRAPPER__GEOSPOLYGONBUFFER__
#define __GEOSWRAPPER__GEOSPOLYGONBUFFER__

#ifdef __cplusplus

#include <CoreGraphics/CGGeometry.h>
#include <vector>

class GEOSPolygonBuffer
{
private:
    GEOSPolygonBuffer() {};
    ~GEOSPolygonBuffer() {};

public:
    
    typedef enum {
        Round,
        Flat,
        Square,
    } Cap;
    
    struct Parameters {
        
        Parameters(float distance, Cap cap = Round, int segments = -1):distance(distance),cap(cap),segments(segments) { };
        
        float distance;
        int segments;
        Cap cap;
    };
    
    static int geos_cap(const Parameters& params);
    
    /**
     Perform buffer operation for polygon of input points.
     
     @param input vector of polygon points.
     
     @param distance buffering offset.
     
     @param output if operation completed - add new buffered polygon points to output (points will be sorted and unique).
     
     @return true if operation succeeded, otherwise false.
     
     @warning returns points may be not convex.
     
     */
    
    static bool buffer(const std::vector<CGPoint>& input, const Parameters params, std::vector<CGPoint>& output);
    
};

#endif /* __cplusplus */
#endif /* defined(__GEOSWRAPPER__GEOSPOLYGONBUFFER__) */
