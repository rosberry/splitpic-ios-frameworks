//
//  GEOSWrapper.h
//  GEOSWrapper
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifdef __cplusplus
#define GEOSWRAPPER_EXTERN extern "C"
#else
#import <UIKit/UIKit.h>
#define GEOSWRAPPER_EXTERN FOUNDATION_EXPORT
#endif

//! Project version number for GEOSWrapper.
GEOSWRAPPER_EXTERN double GEOSWrapperVersionNumber;

//! Project version string for GEOSWrapper.
GEOSWRAPPER_EXTERN const unsigned char GEOSWrapperVersionString[];

#include <GEOSWrapper/GEOSPolygonBuffer.hpp>
#include <GEOSWrapper/GEOSPolygonIntersection.hpp>
#include <GEOSWrapper/GEOSPolygonBufferTriangulation.hpp>
