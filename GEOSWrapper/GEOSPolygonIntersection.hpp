//
//  GEOSPolygonIntersection.hpp
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 10/9/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifndef __GEOSWRAPPER__GEOSPOLYGONINTERSECTION__
#define __GEOSWRAPPER__GEOSPOLYGONINTERSECTION__

#ifdef __cplusplus

#include <CoreGraphics/CGGeometry.h>
#include <vector>

class GEOSPolygonIntersection
{
private:
    GEOSPolygonIntersection() {};
    ~GEOSPolygonIntersection() {};
    
public:
    
    /**
     Perform polygon <-> polygon intersection oepration for polygon of input points.
     
     @param inputs1 vector of first polygon points.
     
     @param inputs2 vector of second polygon points.
     
     @param output if operation completed - add new intersection polygon points to output (points will be sorted and unique).
     
     @return true if operation succeeded, otherwise false.
     
     @warning returns points may be not convex.
     
     */
    
    static bool intersection(const std::vector<CGPoint>& inputs1, const std::vector<CGPoint>& inputs2, std::vector<CGPoint>& output);
    
    /**
     Perform rect <-> polygon intersection oepration for polygon of input points
     (convert rect to vector of points and exec GEOSRectangleIntersection::intersection(vector<CGPoint>&, vector<CGPoint>&, vector<CGPoint>&)).
     
     @param rect input rectangle.
     
     @param inputs vector of polygon points.
     
     @param output if operation completed - add new intersection polygon points to output (points will be sorted and unique).
     
     @return true if operation succeeded, otherwise false.
     
     @warning returns points may be not convex.
     
     */
    
    static bool intersection(const CGRect rect, const std::vector<CGPoint>& inputs, std::vector<CGPoint>& output);
    
};

#endif /* __cplusplus */
#endif /* defined(__GEOSWRAPPER__GEOSRECTANGLEINTERSECTION__) */
