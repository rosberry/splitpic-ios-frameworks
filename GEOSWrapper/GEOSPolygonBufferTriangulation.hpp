//
//  GEOSPolygonBufferTriangulation.hpp
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 12/1/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifndef __GEOSWRAPPER__GEOSPOLYGONBUFFERTRIANGULATION_H__
#define __GEOSWRAPPER__GEOSPOLYGONBUFFERTRIANGULATION_H__

#ifdef __cplusplus

#include "GEOSPolygonBuffer.hpp"
#include <CoreGraphics/CGGeometry.h>
#include <vector>

class GEOSPolygonBufferTriangulation
{
private:
    GEOSPolygonBufferTriangulation() {};
    ~GEOSPolygonBufferTriangulation() {};
    
public:
    
    static bool triangulate(const std::vector<CGPoint>& input, std::vector<CGPoint>& output, const GEOSPolygonBuffer::Parameters params = 0.0f);
};

#endif /* __cplusplus */
#endif /* __GEOSWRAPPER__GEOSPOLYGONBUFFERTRIANGULATION_H__ */
