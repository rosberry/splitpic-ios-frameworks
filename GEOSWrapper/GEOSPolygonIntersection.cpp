//
//  GEOSPolygonIntersection.cpp
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 10/9/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#include "GEOSPolygonIntersection.hpp"

#include <geos/geom/CoordinateArraySequence.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/geom/LinearRing.h>
#include <geos/geom/Polygon.h>
#include <geos/geom/PrecisionModel.h>

using namespace geos::geom;

bool GEOSPolygonIntersection::intersection(const std::vector<CGPoint>& inputs1, const std::vector<CGPoint>& inputs2, std::vector<CGPoint>& output)
{
    bool result = false;
    if (!inputs1.size() || !inputs2.size())
    {
        return result;
    }
    
    auto points1 = std::vector<CGPoint>(inputs1);
    auto points2 = std::vector<CGPoint>(inputs2);
    
    if (!CGPointEqualToPoint(points1.front(), points1.back()))
    {
        points1.push_back(points1[0]);
    }
    
    if (!CGPointEqualToPoint(points2.front(), points2.back()))
    {
        points2.push_back(points2[0]);
    }
    
    auto factory = GeometryFactory::getDefaultInstance();
    
    /* Create Polygone */
    
    CoordinateArraySequence sequence1;
    CoordinateArraySequence sequence2;
    
    for (auto p: points1)
    {
        sequence1.add(Coordinate(p.x, p.y));
    }
    
    for (auto p: points2)
    {
        sequence2.add(Coordinate(p.x, p.y));
    }
    
    auto ring1 = factory->createLinearRing(sequence1);
    auto ring2 = factory->createLinearRing(sequence2);
    
    auto polygon1 = factory->createPolygon(ring1, NULL);
    auto polygon2 = factory->createPolygon(ring2, NULL);
    
    /* Find intersection */
    
    auto intersection = polygon1->intersection(polygon2);
    auto coordinates = intersection->getCoordinates();
    
    if (coordinates)
    {
        auto coord = std::vector<Coordinate>();
        
        for (int i = 0; i < coordinates->size(); i++)
        {
            coord.push_back(coordinates->getAt(i));
        }
        
        /* Sorting coordinates & remove duplicates */
        
        std::sort(coord.begin(), coord.end());
        coord.erase(std::unique(coord.begin(), coord.end()), coord.end());
        
        for (auto c: coord)
        {
            output.push_back(CGPointMake(c.x, c.y));
        }
        
        delete coordinates;
        result = output.size() > 0;
    }
    
    /* Cleanup */
    
    delete intersection;
    delete polygon1;
    delete polygon2;
    
    return result;
}

bool GEOSPolygonIntersection::intersection(const CGRect rect, const std::vector<CGPoint>& inputs, std::vector<CGPoint>& output)
{
    std::vector<CGPoint> input1 = {
        CGPointMake(rect.origin.x, rect.origin.y),
        CGPointMake(rect.origin.x + rect.size.width, rect.origin.y),
        CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height),
        CGPointMake(rect.origin.x, rect.origin.y + rect.size.height),
        CGPointMake(rect.origin.x, rect.origin.y)
    };
    
    return intersection(input1, inputs, output);
}
