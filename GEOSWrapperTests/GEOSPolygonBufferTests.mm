//
//  GEOSPolygonBufferTests.m
//  GEOSWrapperTests
//
//  Created by Andrey Konoplyankin on 9/22/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GEOSUtilities.h"
#import <GEOSWrapper/GEOSWrapper.h>

@interface GEOSPolygonBufferTests : XCTestCase

@end

@implementation GEOSPolygonBufferTests

- (void)testNonCrashForEmptyVectors
{
    auto input = std::vector<CGPoint>();
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonBuffer::buffer(input, 0.0f, output);
}

- (void)testPolygonBuffer
{
    auto input = pointVector(@"0 0 10 0 10 10 0 10");
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonBuffer::buffer(input, 2.0f, output);
    
    XCTAssertEqual(output.size(), 4);
    AssertVectorNotContain(output, @"2 2 8 2 8 8 2 8", true);
}

@end
