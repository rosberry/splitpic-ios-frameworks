//
//  GEOSPolygonBufferTriangulationTests.m
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 12/1/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GEOSUtilities.h"
#import <GEOSWrapper/GEOSWrapper.h>

@interface GEOSPolygonBufferTriangulationTests : XCTestCase

@end

@implementation GEOSPolygonBufferTriangulationTests

- (void)testNonCrashForEmptyVectors
{
    auto input = std::vector<CGPoint>();
    auto output = std::vector<CGPoint>();
    
    bool result = GEOSPolygonBufferTriangulation::triangulate(input, output);
    XCTAssertFalse(result);
}

- (void)testTriangulation
{
    auto input = pointVector(@"0 0 10 0 10 10 0 10");
    auto output = std::vector<CGPoint>();
    
    bool result = GEOSPolygonBufferTriangulation::triangulate(input, output);
    XCTAssertTrue(result);
    
    XCTAssertEqual(output.size(), 6);
    AssertVectorNotContain(output, @"0 0 10 0 10 10 0 10");
}

- (void)testBufferTriangulation
{
    auto input = pointVector(@"20 20 300 20 300 300 20 300");
    auto output = std::vector<CGPoint>();
    
    bool result = GEOSPolygonBufferTriangulation::triangulate(input, output, 20.0f);
    XCTAssertTrue(result);
    /*
    UIGraphicsBeginImageContext(CGSizeMake(320, 320));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (int i = 0; i < output.size(); i += 3)
    {
        CGContextMoveToPoint(context, output[i].x, output[i].y);
        CGContextAddLineToPoint(context, output[i + 1].x, output[i + 1].y);
        CGContextAddLineToPoint(context, output[i + 2].x, output[i + 2].y);
        CGContextClosePath(context);
        CGContextStrokePath(context);
    }
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    */
    XCTAssertEqual(output.size(), 24);
}

@end
