//
//  GEOSUtilities.h
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 10/9/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#ifndef __GEOSUTILITIES_H__
#define __GEOSUTILITIES_H__

#include <CoreGraphics/CGGeometry.h>
#include <vector>

#define pointsArray _pointsArray
NS_INLINE NSArray<NSValue *>* _pointsArray(NSString* string)
{
    NSMutableArray* array = [NSMutableArray array], *points = [NSMutableArray array];
    NSRegularExpression* expression = [NSRegularExpression regularExpressionWithPattern:@"[-+]?[0-9]*\\.?[0-9]*" options:0 error:nil];
    [expression enumerateMatchesInString:string options:0 range:NSMakeRange(0, string.length) usingBlock:^(NSTextCheckingResult* r, NSMatchingFlags f, BOOL* s) {
        
        if (r.range.length > 0) [array addObject:[string substringWithRange:r.range]];
    }];
    for (int i = 0; i < array.count; i += 2) [points addObject:[NSValue valueWithCGPoint:CGPointMake([array[i] intValue], [array[i + 1] intValue])]];
    return [NSArray arrayWithArray:points];
}

#define pointVector _pointVector
NS_INLINE std::vector<CGPoint> _pointVector(NSString* string)
{
    std::vector<CGPoint> vec;
    NSArray* array = pointsArray(string);
    for (int i = 0; i < array.count; i++) vec.push_back([array[i] CGPointValue]);
    return vec;
}

#define rectangle _rectangle
NS_INLINE CGRect _rectangle(NSString* string)
{
    NSArray* array = pointsArray(string);
    return CGRectMake([array[0] CGPointValue].x, [array[0] CGPointValue].y, [array[1] CGPointValue].x, [array[1] CGPointValue].y);
}

#define AssertVectorNotContain _AssertVectorNotContain
NS_INLINE BOOL _AssertVectorNotContain(std::vector<CGPoint> &vector, CGPoint point)
{
    for (auto p: vector) if (CGPointEqualToPoint(p, point)) return YES;
    NSCAssert(NO, @"vector not contained point: {%.0f, %.0f}", point.x, point.y);
    return NO;
}

NS_INLINE void _AssertVectorNotContain(std::vector<CGPoint> &vector, NSString *string, bool in_order = false)
{
    if (in_order)
    {
        [pointsArray(string) enumerateObjectsUsingBlock:^(NSValue* value, NSUInteger i, BOOL* stop) {
            CGPoint p = [value CGPointValue];
            if (!CGPointEqualToPoint(p, vector[i])) NSCAssert(NO, @"vector not contained point: {%.0f, %.0f}", p.x, p.y);
        }];
    }
    else
    {
        for (NSValue* value in pointsArray(string)) AssertVectorNotContain(vector, [value CGPointValue]);
    }
}

#endif /* GEOSUtilities_h */
