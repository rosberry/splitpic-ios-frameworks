//
//  GEOSUtilitiesTests.m
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 10/9/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GEOSUtilities.h"

@interface GEOSUtilitiesTests : XCTestCase

@end

@implementation GEOSUtilitiesTests

- (void)testPointsArray
{
    NSArray* points = pointsArray(@"10 20 -10 -20 13 0");
    
    XCTAssertEqual(points.count, 3);
    XCTAssertEqualObjects(points[0], [NSValue valueWithCGPoint:CGPointMake(10, 20)]);
    XCTAssertEqualObjects(points[1], [NSValue valueWithCGPoint:CGPointMake(-10, -20)]);
    XCTAssertEqualObjects(points[2], [NSValue valueWithCGPoint:CGPointMake(13, 0)]);
}

- (void)testPointVector
{
    auto vec = pointVector(@"10 20 -10 -20 13 0");
    
    XCTAssertEqual(vec.size(), 3);
    XCTAssert(CGPointEqualToPoint(vec[0], CGPointMake(10, 20)));
    XCTAssert(CGPointEqualToPoint(vec[1], CGPointMake(-10, -20)));
    XCTAssert(CGPointEqualToPoint(vec[2], CGPointMake(13, 0)));
}

- (void)testRectangle
{
    CGRect rect = rectangle(@"0 1 2 3");
    XCTAssert(CGRectEqualToRect(rect, CGRectMake(0, 1, 2, 3)));
}

- (void)testVectorContain
{
    auto vec = pointVector(@"10 20 -10 -20 13 0");
    
    AssertVectorNotContain(vec, CGPointMake(10, 20));
    AssertVectorNotContain(vec, CGPointMake(-10, -20));
    AssertVectorNotContain(vec, CGPointMake(13, 0));
}

- (void)testVectorContainString
{
    auto vec = pointVector(@"10 20 -10 -20 13 0");
    
    AssertVectorNotContain(vec, @"13 0 -10 -20 10 20");
}

@end
