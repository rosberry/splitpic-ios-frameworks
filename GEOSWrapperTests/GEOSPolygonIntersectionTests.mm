//
//  GEOSPolygonIntersectionTests.m
//  SplitPicDynamicFrameworks
//
//  Created by Andrey Konoplyankin on 10/9/15.
//  Copyright © 2015 rosberry. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GEOSUtilities.h"
#import <GEOSWrapper/GEOSWrapper.h>

@interface GEOSPolygonIntersectionTests : XCTestCase

@end

@implementation GEOSPolygonIntersectionTests

- (void)testNonCrashForEmptyVectors
{
    auto input1 = std::vector<CGPoint>();
    auto input2 = std::vector<CGPoint>();
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonIntersection::intersection(input1, input2, output);
    GEOSPolygonIntersection::intersection(CGRectZero, input1, output);
}

- (void)testRectangleIntersection
{
    auto input = pointVector(@"-1 1 5 1 5 3 -1 3");
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonIntersection::intersection(rectangle(@"0 0 4 4"), input, output);
    
    XCTAssertEqual(output.size(), 4);
    AssertVectorNotContain(output, @"0 1 4 1 4 3 0 3");
}

- (void)testRectanglePartial4PointIntersection
{
    auto input = pointVector(@"-1 -1 5 -1 5 1 -1 1");
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonIntersection::intersection(rectangle(@"0 0 4 4"), input, output);
    
    XCTAssertEqual(output.size(), 4);
    AssertVectorNotContain(output, @"0 0 4 0 0 1 4 1");
}

- (void)testRectanglePartial5PointIntersection
{
    auto input = pointVector(@"-2 -1 2 -5 6 -1 2 3");
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonIntersection::intersection(rectangle(@"0 0 4 4"), input, output);
    
    XCTAssertEqual(output.size(), 5);
    AssertVectorNotContain(output, @"0 0 4 0 4 1 2 3 0 1");
}

- (void)testRotatedRectangleIntersection
{
    auto input = pointVector(@"-2 -2 2 -6 6 -2 2 2");
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonIntersection::intersection(rectangle(@"0 0 4 4"), input, output);
    
    XCTAssertEqual(output.size(), 3);
    AssertVectorNotContain(output, @"0 0 4 0 2 2");
}

- (void)testRectangleInsideInRect
{
    auto input = pointVector(@"1 1 3 1 3 3 1 3");
    auto output = std::vector<CGPoint>();
    
    GEOSPolygonIntersection::intersection(rectangle(@"0 0 4 4"), input, output);
    
    XCTAssertEqual(output.size(), 4);
    AssertVectorNotContain(output, @"1 1 3 1 3 3 1 3");
}

- (void)testRectangleOutsideRect
{
    auto input = pointVector(@"5 5 10 5 10 10 5 10");
    auto output = std::vector<CGPoint>();
    
    bool result = GEOSPolygonIntersection::intersection(rectangle(@"0 0 4 4"), input, output);
    
    XCTAssertEqual(output.size(), 0);
    XCTAssertFalse(result);
}

@end
